using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class GameManager : MonoBehaviour {

	public GameObject seaPrefab;

	public Text shipsLeftTextBox;
	public Text shotsLeftTextBox;

	public int ShipsLeft {get; set;}
	public int ShotsLeft {get; set;}


	void Start () {
		buildLevel();

		ShipsLeft = FindObjectsOfType<AircraftCarrier>().Length / 5;
		ShipsLeft += FindObjectsOfType<Battleship>().Length / 4;
		ShipsLeft += FindObjectsOfType<Cruiser>().Length / 3;
		ShipsLeft += FindObjectsOfType<Destroyer>().Length / 2;
		ShipsLeft += FindObjectsOfType<Submarine>().Length;

		shipsLeftTextBox.text = "Ships Left: " + ShipsLeft;

		ShotsLeft = 15;

		shotsLeftTextBox.text = "Shots Left: " + ShotsLeft;
	}
	
	private void buildLevel() {
		int[] currentLayout = ShipLayouts.level1;

		int rows = 0;
		int column = 0;

		for(int i = 0; i < currentLayout.Length; i++) {
			GameObject square = Instantiate (seaPrefab) as GameObject;

			switch (currentLayout[i]){
			case 0:
				square.AddComponent<Sea>();
				break;
			case 1:
				square.AddComponent<Submarine>();
				break;
			case 2:
				square.AddComponent<Submarine>();
				break;
			case 3:
				square.AddComponent<Submarine>();
				break;
			case 4:
				square.AddComponent<Submarine>();
				break;
			case 5:
				square.AddComponent<Destroyer>();
				break;
			case 6:
				square.AddComponent<Destroyer>();
				break;
			case 7:
				square.AddComponent<Cruiser>();
				break;
			case 8:
				square.AddComponent<Cruiser>();
				break;
			case 9:
				square.AddComponent<Battleship>();
				break;
			case 10:
				square.AddComponent<AircraftCarrier>();
				break;
				
			}
			square.GetComponent<BoardSquare>().Number=currentLayout[i];
			square.transform.position = new Vector3 (-4.5f+column*(square.GetComponent<Renderer>().bounds.size.x),
			                                        3.5f - ((square.GetComponent<Renderer>().bounds.size.y) * rows),
			                                        0);

			column++;
			if (column==10) {
				column = 0;
				rows++;
			}
		}
	}

	public void destroyShip() {
		ShipsLeft--;
		ShotsLeft += 4;
		shotsLeftTextBox.text = "Shots Left: " + ShotsLeft;
		shipsLeftTextBox.text = "Ships Left: " + ShipsLeft;
	}

	//this method will delete 1 from Shots Left variable when called
	public void shotFired() {
		ShotsLeft--;
		shotsLeftTextBox.text = "Shots Left: " + ShotsLeft;
	}

}
